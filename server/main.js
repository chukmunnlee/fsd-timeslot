const { join } = require('path')
const MongoClient = require('mongodb').MongoClient;
const morgan = require('morgan')
const express = require('express')

const api = require('./api')

const DATABASE = 'fsd2020'
const ANGULAR_DIST = 'dist'

const PORT = parseInt(process.env.PORT) || 3000
const MONGO_URL = process.env.MONGO_URL || 'mongodb://localhost:27017/fsd2020'

const client = new MongoClient(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true})

const app = express()

app.use(morgan('combined'))

app.use('/api', express.json(), api(client))

app.use(express.static(join(__dirname, ANGULAR_DIST)))

client.connect()
	.then(() => client.db(DATABASE).command({ ping: 1 }))
	.then(() => {
		app.listen(PORT, () => {
			console.info(`Application started on port ${PORT} at ${new Date()}`)
			const idx = MONGO_URL.indexOf(':')
			console.info(`\t${MONGO_URL.substring(0, idx)}`)
		})
	})
	.catch(error => {
		console.error('Cannot start application\n', error)
	})
