const { findBookedSlotsByGroup,
	findSlotByEmail, findSlotById,
	findParticipantByEmail,
	updateBookings
} = require('./database')

const { createHash } = require('crypto')

const express = require('express')

const sendError = (code, errObj, resp) =>
	resp.status(code).type('application/json')
		.json({ error: errObj })

const sendResult = (code, result, resp) =>
	resp.status(code).type('application/json')
		.json(result)


module.exports = (client) => {

	const router = express.Router()

	router.get('/participant',
		(req, resp) => {
			const email = req.query['email']
			if (!email) 
				return sendError(400, `Missing email`, resp)

			Promise.all([
				findParticipantByEmail(email.trim(), client),
				findSlotByEmail(email.trim(), client)
			]).then(result => {
				const [ part, slot ] = result
				if (!part)
					return sendError(400, `Cannot find ${email}`, resp)

				const doc = {
					id: part._id.toString(),
					email: part.email,
					name: part.name,
					group: part.group,
				}

				if (!!slot)
					//TODO append slot 
					doc.slot = { day: slot.day, slot: slot.slot, track: slot.track, meet: slot.meet }

				sendResult(200, doc, resp)
			})
			.catch(error => {
				sendError(500, error, resp)
			})
		}
	)

	router.get('/slot/booked/:groupId',
		(req, resp) => {
			let groupId = parseInt(req.params.groupId) 
			if (isNaN(groupId))
				groupId = -1
			findBookedSlotsByGroup(groupId, client)
				.then(result => {
					result = result? result: []
					doc = {
						group: groupId,
						booked: result
					}
					sendResult(200, doc, resp)
				})
				.catch(error => {
					sendError(500, error, resp)
				})
		}
	)

	// not using :id yet
	router.put('/slot/:id',
		(req, resp) => {
			const updateBooked = req.body
			updateBooked.password = createHash('sha256')
					.update(updateBooked.password, 'utf8').digest('hex');

			((updateBooked.day < 0)? Promise.resolve(null): findSlotByEmail(updateBooked.email, client))
				.then(result => {
					if (!!result)
						return [ result.password == updateBooked.password, result ]
					return [ true, result ]
				})
				.then(result => {
					const [ passwd, doc ] = result
					if (!passwd)
						return null;

					let payload = updateBooked
					if (!!doc) {
						payload.track = doc.track
						payload.slot = doc.slot
						payload.day = doc.day
					}
					return updateBookings(payload, client)
				})
				.then(result => {
					if (!result)
						return sendError(403, 'Incorrect password', resp)
					sendResult(200, result, resp)
				})
				.catch(error => {
					sendError(500, error, resp)
				})
		}
	)

	router.get('/slot/:oId',
		(req, resp) => {
			findSlotById(req.params.oId, client)
				.then(result => {
				})
				.catch(error => {
					sendError(500, error, resp)
				})
		}
	)

	router.use((req, resp) => 
		sendError(400, `${req.originalUrl} not found`, resp)
	)

	return router
}
