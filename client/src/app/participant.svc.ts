import {HttpClient, HttpParams} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";

import { Booked, BookedResponse, Participant, UpdateBooked } from './models'

@Injectable()
export class ParticipantService implements CanActivate {

	participant: Participant = null;

	constructor(private http: HttpClient, private router: Router) { }

	clear() {
		this.participant = null
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		if (!this.participant)
			return this.router.parseUrl('/')

		return (true)
	}

	updateBooking(id: string, book: UpdateBooked): Promise<any> {
		return this.http.put(`/api/slot/${id}`, book)
			.toPromise()
	}

	findParticipantByEmail(email: string): Promise<Participant> {
		const params = new HttpParams().set('email', email)
		return this.http.get<Participant>('/api/participant', { params })
			.toPromise()
			.then(result => {
				this.participant = result
				return result
			})
	}

	findBookedSlots(groupId: number): Promise<BookedResponse> {
		return this.http.get<BookedResponse>(`/api/slot/booked/${groupId}`)
			.toPromise()
	}
}
