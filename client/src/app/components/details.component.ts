import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {DAYS, GROUP, Participant, SLOTS, SlotTable, Track, UpdateBooked} from '../models';
import {ParticipantService} from '../participant.svc';
import {mkSlotTable, setSlotTable, timeMajor, updateSlotTable} from '../utils';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

	participant: Participant = null
	form: FormGroup

	groupName = ""
	selectedDay = "None"
	selectedTrack = "None"
	selectedSlot = "None"

	errorText = null

	//prevSelection
	prevDay = -1
	prevSlot = -1
	prevTrack = -1
	//current selection
	currDay = -1
	currSlot = -1
	currTrack = -1

	slotTable: SlotTable
	byTimeMajor: Track[][] = []

	DAYS: string[] = []
	SLOTS = SLOTS

	constructor(private fb: FormBuilder, private router: Router
	  	, private partSvc: ParticipantService) { }

	ngOnInit(): void { 
		this.participant = this.partSvc.participant;

		if (!this.participant)
			return this.back()

		this.groupName = GROUP[this.participant.group]

		this.DAYS = DAYS[this.participant.group]

		this.partSvc.findBookedSlots(this.participant.group)
			.then(result => {
				this.slotTable = mkSlotTable(this.participant.group)
				this.slotTable = updateSlotTable(this.slotTable, result.booked)
				if ('slot' in this.participant) {
					this.prevDay = this.participant.slot.day
					this.prevSlot = this.participant.slot.slot
					this.prevTrack = this.participant.slot.track
					this.currDay = this.prevDay
					this.currSlot = this.prevSlot
					this.currTrack = this.prevTrack
					setSlotTable(
						{ day: this.prevDay, slot: this.prevSlot, track: this.prevTrack },
						this.slotTable, true
					)
					this.updateSlotLabel(this.prevDay, this.prevSlot, this.prevTrack)
				}
				this.byTimeMajor = timeMajor(this.slotTable)
			})

		this.form = this.fb.group({
			meet: this.fb.control('', [ Validators.required ]),
			password0: this.fb.control('', [ Validators.required ]),
			password1: this.fb.control('', [ Validators.required ])
		})
	}

	canUpdate() {
		return this.form.valid && (this.currDay >= 0) && (
			(this.currDay != this.prevDay)
			|| (this.currSlot != this.prevSlot)
			|| (this.currTrack != this.prevTrack)
		)
	}

	updateBooking() {
		this.errorText = null
		const pw0 = this.form.get('password0').value
		const pw1 = this.form.get('password1').value
		if (pw0 != pw1) {
			this.errorText = 'Password do not match'
			return
		}
		//TODO: make call to update
		const updateBooking: UpdateBooked = {
			email: this.participant.email,
			group: this.participant.group,
			day: this.prevDay,
			slot: this.prevSlot,
			track: this.prevTrack,
			password: pw0,
			updateDay: this.currDay,
			updateSlot: this.currSlot,
			updateTrack: this.currTrack,
			meet: this.form.get('meet').value
		}

		this.partSvc.updateBooking(this.participant.id, updateBooking)
			.then(resp => this.router.navigate(['/']))
			.catch(error => {
				const errObj = error.error
				this.errorText = 'error' in errObj? errObj.error: JSON.stringify(errObj)
			})
	}

	selectSlot(day: number, slot: number, track: number) {
		this.currDay = day;
		this.currSlot = slot;
		this.currTrack = track;

		this.updateSlotLabel(day, slot, track)
	}

	updateSlotLabel(day: number, slot: number, track: number) {
		this.selectedDay = this.DAYS[day]
		this.selectedSlot = this.SLOTS[slot]
		this.selectedTrack = (track == 0)? 'A': 'B'
	}

	showSelection(day: number, slot: number, track: number) {
		//const v = (this.currDay == day) && (this.currSlot = slot) && (this.currTrack == track)
		//console.info(`showSelection: day: ${day}, slot: ${slot}, track: ${track}: result: ${v}`)
		return (this.currDay == day) && (this.currSlot == slot) && (this.currTrack == track)
	}

	back() {
		this.router.navigate([ '/' ])
	}

}
