import {HttpErrorResponse} from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ParticipantService} from '../participant.svc';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

	form: FormGroup
	errorText: string = ""
	hasError = false

	constructor(private fb: FormBuilder, private router: Router
			, private partSvc: ParticipantService) { }
	
	ngOnInit(): void { 
		this.partSvc.clear()
		this.form = this.fb.group({
			email: this.fb.control('', [ Validators.required, Validators.email ])
		})
	}

	findUser() {
		this.hasError = false
		this.errorText = ""
		const email = this.form.value['email'].trim()
		this.partSvc.findParticipantByEmail(email)
			.then(() => this.router.navigate([ '/detail' ]))
			.catch((errResp: HttpErrorResponse) => {
				console.error('Error: ', errResp)
				this.hasError = true;
				this.errorText = errResp.error['error']
			})
	}

}
