export const GROUP = [ 'IBF', 'PCP/SGUS' ]
export const TRACK = [ 'A', 'B' ]
export const DAYS = [
	// IBF
	[ 'May 10', 'May 11', 'May 12' ], // may 10, 11, 12
	// PCP/SGUS
	[ 'Apr 22' ] // apr 22
]

export const SLOTS = [ 
	'0900 - 0930', '0930 - 1000', '1000 - 1030', '1030 - 1100', '1100 - 1130',
	'1330 - 1400', '1400 - 1430', '1430 - 1500', '1500 - 1530', '1530 - 1600'
]

export interface Participant {
	id: string
	email: string 
	name: string 
	group: number 
	slot: Slot
}

export interface Slot {
	day: number
	slot: number
	track: number
	email?: string
	meet?: string
}

export interface Track {
	track: boolean[]
}

export interface TimeSlot {
	slots: Track[]
}

export interface SlotTable {
	days: TimeSlot[]
}

export interface Booked {
	email: string
	group: number
	day: number
	slot: number
	track: number
}

export interface BookedResponse {
	group: number
	booked: Booked[]
}

export interface UpdateBooked extends Booked {
	password: string
	updateDay: number
	updateSlot: number
	updateTrack: number
	meet: string
}
