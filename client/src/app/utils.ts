import {Booked, DAYS, SLOTS, SlotTable, TimeSlot, Track, Slot} from "./models"



export const mkSlotTable = (group: number) => {
	const table: SlotTable = { days: [] }
	for (let i = 0; i < DAYS[group].length; i++) {
		const s = []
		for (let j = 0; j < SLOTS.length; j++)
			s.push([ true, true ])
		// not good! filled with the same instance
		//const s = Array(SLOTS.length).fill([true, true]) as Track[]
		table.days.push({ slots: s } as TimeSlot)
	}
	return table
}

export const updateSlotTable = (table: SlotTable, booked: Booked[]) => {
	booked.forEach(b => {
		const day = b.day
		const slot = b.slot
		const track = b.track
		//table.days[day].slots[slot][track] = false
		setSlotTable({ day, slot, track }, table, false)
	})
	return table
}

export const setSlotTable = (timeSlot: Slot, table: SlotTable, value: boolean) => {
	const { day, slot, track } = timeSlot
	table.days[day].slots[slot][track] = value
}

export const timeMajor = (table: SlotTable) => {
	const byTime: Track[][] = []
	for (let i = 0; i < SLOTS.length; i++) {
		const slots = []
		for (let j of table.days)
			slots.push(j.slots[i])
		byTime.push(slots)
	}
	return byTime
}
