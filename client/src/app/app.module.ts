import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { RouterModule, Routes } from '@angular/router'

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MainComponent } from './components/main.component';
import {ParticipantService} from './participant.svc';
import { DetailsComponent } from './components/details.component';

const ROUTES: Routes = [
	{ path: '', component: MainComponent },
	{ path: 'detail', component: DetailsComponent, canActivate: [ ParticipantService ] },
	{ path: '**', redirectTo: '/', pathMatch: 'full' }
]

@NgModule({
	declarations: [
		AppComponent,
		MainComponent,
		DetailsComponent
	],
	imports: [
		BrowserModule, HttpClientModule,
		FormsModule, ReactiveFormsModule,
		RouterModule.forRoot(ROUTES),
		NgbModule
	],
		providers: [ ParticipantService ],
		bootstrap: [AppComponent]
})
export class AppModule { }
