const MongoClient = require('mongodb').MongoClient

const client = new MongoClient(process.env.MONGO_URL, 
	{ useNewUrlParser: true, useUnifiedTopology: true })

client.connect()
	.then(() => client.db('fsd2020').collection('slots')
		.find({ group: 0 })
		.project({ _id: 0, email: 1, day: 1, slot: 1, track: 1, meet: 1 })
		.toArray())
	.then(result => result.map(v => Object.values(v)))
	.then(result => result.map(v => v.join(',')))
	.then(result => result.forEach(v => console.info(v)))
	.then(() => {
		client.close(() => {})
		process.exit(0)
	})
