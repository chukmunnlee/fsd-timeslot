FROM node:15 AS ngbuild

ENV CLIENT_DIR="/app"

WORKDIR ${CLIENT_DIR}

RUN git clone https://gitlab.com/chukmunnlee/fsd-timeslot.git 

RUN npm i -g @angular/cli 

WORKDIR ${CLIENT_DIR}/fsd-timeslot/client

RUN npm i
RUN ng build

FROM node:15

ENV APP_DIR="/app" BUILDER_ROOT="/app/fsd-timeslot" PORT=3000

WORKDIR ${APP_DIR}

COPY --from=ngbuild ${BUILDER_ROOT}/server/api.js .
COPY --from=ngbuild ${BUILDER_ROOT}/server/database.js .
COPY --from=ngbuild ${BUILDER_ROOT}/server/main.js .
COPY --from=ngbuild ${BUILDER_ROOT}/server/package.json .
COPY --from=ngbuild ${BUILDER_ROOT}/server/package-lock.json .
COPY --from=ngbuild ${BUILDER_ROOT}/client/dist/client dist

RUN npm i

EXPOSE ${PORT}

ENTRYPOINT [ "node", "main.js" ]
